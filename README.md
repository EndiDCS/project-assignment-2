# Project Assignment 2

Image Processing - SCC5830

Assignment 2 - Image Enhancement and Filtering

Author:
* Endi Daniel Coelho Silva

Folders and files:
* [Python code](./submission/assignment2.py) contains the Python code used for run.codes submission
* [Images](/images) contains images used in the demos
* [Notebook with Demo](assignment_2.ipynb) is a notebook exemplifying functions developed and submitted
